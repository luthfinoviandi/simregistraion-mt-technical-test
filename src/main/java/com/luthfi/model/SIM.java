package com.luthfi.model;

import com.opencsv.bean.CsvBindByName;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

public class SIM {

    @CsvBindByName(column ="MSISDN")
    private String MSISDN;

    @CsvBindByName(column ="SIM_TYPE")
    private String simType;

    @CsvBindByName(column ="NAME")
    private String name;

    @CsvBindByName(column ="DATE_OF_BIRTH")
    private Date dateOfBirth;

    @CsvBindByName(column ="GENDER")
    private String gender;

    @CsvBindByName(column ="ADDRESS")
    private String address;

    @CsvBindByName(column ="ID_NUMBER")
    private String idNumber;

    public String getMSISDN() {
        return MSISDN;
    }

    public void setMSISDN(String MSISDN) {
        this.MSISDN = MSISDN;
    }

    public String getSimType() {
        return simType;
    }

    public void setSimType(String simType) {
        this.simType = simType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }
}
