package com.luthfi.service;

import com.luthfi.constant.GenreConst;
import com.luthfi.constant.SIMTypeConst;
import com.luthfi.model.SIM;
import com.luthfi.repository.SIMRepository;
import com.luthfi.util.CsvUtil;
import com.luthfi.util.ValidationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SIMService {

    private static final Logger logger = LoggerFactory.getLogger(SIMService.class);

    public void writeCsv() throws IOException, ParseException {

        InputStream inputStream = new FileInputStream("sim.csv");
        List<SIM> simList = SIMRepository.parseCsvFile(inputStream);

        SIM sim = new SIM();
        sim.setMSISDN("+66" + CsvUtil.getRandomNumber(100000, 999999));
        sim.setName(CsvUtil.getAlphanumericRandom(10, true, false));
        sim.setIdNumber(CsvUtil.getAlphanumericRandom(12, true, true));
        sim.setAddress(CsvUtil.getAlphanumericRandom(19, true, false));
        sim.setDateOfBirth(
                CsvUtil.getRandomDate(CsvUtil.stringToDate("dd-MM-yyyy", "01-01-1981"), new Date())
        );

        int randomNum = CsvUtil.getRandomNumber(1, 4);
        if(randomNum % 2 == 0){
            sim.setGender(GenreConst.MALE.getValue());
            sim.setSimType(SIMTypeConst.PREPAID.getValue());
        } else {
            sim.setGender(GenreConst.FEMALE.getValue());
            sim.setSimType(SIMTypeConst.POSTPAID.getValue());
        }

        simList.add(sim);
        SIMRepository.writeToCsv(simList);
    }

    public void register(InputStream file) throws IOException{
        try{
            List<SIM> simList = SIMRepository.parseCsvFile(file);
            List<String> processedMSISDN = new ArrayList<>();
            for(SIM sim : simList){

                try {
                    if(validateRow(sim, processedMSISDN)) {
                        String fileContent = String.format("%s, %s, %s, %s, %s, %s, %s",
                                sim.getMSISDN(),
                                sim.getSimType(),
                                sim.getName(),
                                sim.getDateOfBirth(),
                                sim.getGender(),
                                sim.getAddress(),
                                sim.getIdNumber());

                        FileWriter fileWriter = new FileWriter(sim.getMSISDN() + ".txt");
                        fileWriter.write(fileContent);
                        fileWriter.close();

                        sendSMS(sim);
                        processedMSISDN.add(sim.getMSISDN());
                    }
                } catch (IOException e){
                    logger.error("Error writing file!");
                    e.printStackTrace();
                }
            }
        } catch (Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    private void sendSMS(SIM sim){
        if(sim.getGender().equalsIgnoreCase("M")){
            logger.info("Hi Mr. " + sim.getName() + ", thank you for register your SIM!");
        } else {
            logger.info("Hi Mrs. " + sim.getName() + ", thank you for register your SIM!");
        }
    }

    private boolean validateRow(SIM sim, List<String> processedMSISDN) {
        if (isNotNullOrEmpty(sim.getMSISDN())
            && isNotNullOrEmpty(sim.getAddress())
            && isNotNullOrEmpty(sim.getGender())
            && isNotNullOrEmpty(sim.getIdNumber())
            && isNotNullOrEmpty(sim.getName())
            && isNotNullOrEmpty(sim.getSimType())
            && sim.getDateOfBirth() != null) {

            if(processedMSISDN.size() > 0) {
                int findNumber = processedMSISDN.stream()
                        .filter(str -> str.trim().contains(sim.getMSISDN()))
                        .collect(Collectors.toList()).size();

                if(findNumber > 0) {
                    System.out.println("Registration rejected, duplicate MSISDN!");
                    return false;
                }
            }

            if (ValidationUtil.haveSpecialChar(sim.getName())) {
                System.out.println("Registration rejected, name shouldn't have any special character!");
                return false;
            }

            if (!ValidationUtil.dateNotTodayFuture(sim.getDateOfBirth())) {
                System.out.println("Registration rejected, date of birth shouldn't be Today or Future!");
                return false;
            }

            if (!sim.getGender().equals("M")
                    && !sim.getGender().equals("F")) {
                System.out.println("Registration rejected, wrong gender value!");
                return false;
            }

            if (sim.getAddress().length() > 20) {
                System.out.println("Registration rejected, too many character of address!");
                return false;
            }

            if (!ValidationUtil.haveDigitAndLetter(sim.getIdNumber())) {
                System.out.println("Registration rejected, wrong id number format!");
                return false;
            }

            if (!sim.getSimType().equals("PREPAID")
                    && !sim.getSimType().equals("POSTPAID")) {
                System.out.println("Registration rejected, wrong sim type value!");
                return false;
            }

            if (!ValidationUtil.phoneFormat(sim.getMSISDN())) {
                System.out.println("Registration rejected, wrong MSISDN type format!");
                return false;
            }

            return true;
        } else {
            System.out.println("Registration rejected, cannot use empty value!");
            return false;
        }


    }

    private boolean isNotNullOrEmpty(String value) {
        if(value != null && !value.isEmpty())
            return true;
        else
            return false;
    }
}
