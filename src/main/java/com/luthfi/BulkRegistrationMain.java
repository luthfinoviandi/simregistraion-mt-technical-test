package com.luthfi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class BulkRegistrationMain {

    public static void main(String[] args) {
        SpringApplication.run(BulkRegistrationMain.class, args);
    }

}