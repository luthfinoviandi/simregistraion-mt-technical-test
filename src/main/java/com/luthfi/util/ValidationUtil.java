package com.luthfi.util;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationUtil {
    public static boolean haveSpecialChar(String value){
        Pattern pattern = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(value);

        return matcher.find();
    }

    public static boolean haveDigitAndLetter(String value){
        return value.matches("(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,15})$");
    }

    public static boolean dateNotTodayFuture(Date value){
        if(value.before(new Date())){
            return true;
        } else {
            return false;
        }
    }

    public static boolean phoneFormat(String value){
        Pattern pattern = Pattern.compile("^\\+(?:[0-9]?){6,14}[0-9]$");

        Matcher matcher = pattern.matcher(value);

        return matcher.matches();
    }
}
