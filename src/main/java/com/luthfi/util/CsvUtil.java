package com.luthfi.util;

import com.luthfi.model.SIM;
import com.luthfi.repository.SIMRepository;
import com.opencsv.CSVWriter;
import com.opencsv.bean.*;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;

public class CsvUtil {

    private static final Logger logger = LoggerFactory.getLogger(CsvUtil.class);

    public static final Map<String, SimpleDateFormat> cache = new ConcurrentHashMap<String, SimpleDateFormat>(5);

    private static String csvExtension = "csv";
    public static boolean isCSVFile(MultipartFile file) {
        String extension = file.getOriginalFilename().split("\\.")[1];

        if(!extension.equals(csvExtension)) {
            return false;
        }

        return true;
    }

    public static Date stringToDate(String format, String value) throws ParseException {
        Date date = new SimpleDateFormat(format).parse(value);

        return date;
    }

    public static String formatDate(Date date, String pattern) {
        String result = "";
        try {
            SimpleDateFormat formatter = cache.get(pattern);
            if (null == formatter) {
                formatter = new SimpleDateFormat(pattern);
                cache.put(pattern, formatter);
            }
            if (date != null) {
                result = formatter.format(date);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return result;
    }

    public static int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

    public static String getAlphanumericRandom(int length, boolean useLetters, boolean useNumbers){
        String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);

        return generatedString;

    }

    public static Date getRandomDate(Date start, Date end) {
        long startDate = start.getTime();
        long endDate = end.getTime();
        long randomDate = ThreadLocalRandom
                .current()
                .nextLong(startDate, endDate);

        return new Date(randomDate);
    }
}
