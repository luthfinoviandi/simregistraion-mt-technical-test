package com.luthfi.repository;

import com.luthfi.model.SIM;
import com.luthfi.util.CsvUtil;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class SIMRepository {
    private static final Logger logger = LoggerFactory.getLogger(SIMRepository.class);

    public static List<SIM> parseCsvFile(InputStream inputStreams) {
        BufferedReader fileReader = null;
        CSVParser csvParser = null;
        //CSVFormat csvFormat = CSVFormat.newFormat(';');

        List<SIM> simList = new ArrayList<SIM>();

        try {
            fileReader = new BufferedReader(new InputStreamReader(inputStreams, "UTF-8"));
            csvParser = new CSVParser(fileReader,
                    CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            for (CSVRecord csvRecord : csvRecords) {
                SIM sim = new SIM();
                sim.setMSISDN(csvRecord.get("MSISDN"));
                sim.setSimType(csvRecord.get("SIM_TYPE"));
                sim.setName(csvRecord.get("NAME"));
                sim.setAddress(csvRecord.get("ADDRESS"));
                sim.setDateOfBirth(CsvUtil.stringToDate("dd/MM/yy", csvRecord.get("DATE_OF_BIRTH")));
                sim.setGender(csvRecord.get("GENDER"));
                sim.setIdNumber(csvRecord.get("ID_NUMBER"));
                simList.add(sim);
            }

        } catch (Exception e) {
            System.out.println("Reading CSV Error!");
            e.printStackTrace();
        } finally {
            try {
                fileReader.close();
                csvParser.close();
            } catch (IOException e) {
                System.out.println("Closing fileReader/csvParser Error!");
                e.printStackTrace();
            }
        }

        return simList;
    }

    public static void writeToCsv(List<SIM> simList) throws IOException {
        BufferedWriter writer = Files.newBufferedWriter(Paths.get("sim.csv"));
        //CSVFormat csvFormat = CSVFormat.newFormat(';');

        try (CSVPrinter csvPrinter = new CSVPrinter(writer,
                CSVFormat.DEFAULT.withHeader("MSISDN", "SIM_TYPE", "NAME", "DATE_OF_BIRTH", "GENDER", "ADDRESS", "ID_NUMBER"))) {
            for (SIM sim : simList) {
                csvPrinter.printRecord(
                        sim.getMSISDN(),
                        sim.getSimType(),
                        sim.getName(),
                        CsvUtil.formatDate(sim.getDateOfBirth(), "dd/MM/yy"),
                        sim.getGender(),
                        sim.getAddress(),
                        sim.getIdNumber()
                );
            }
            csvPrinter.flush();
        } catch (Exception e) {
            System.out.println("Writing CSV error!");
            e.printStackTrace();
        }
    }

}
