package com.luthfi.cron;

import com.luthfi.repository.SIMRepository;
import com.luthfi.service.SIMService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.core.support.EventPublishingRepositoryProxyPostProcessor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.ParseException;

@Component
public class CsvWriterTask {

    @Autowired
    SIMService simService;

    private static final Logger logger = LoggerFactory.getLogger(SIMRepository.class);

    @Scheduled(cron = "0/20 * * * * ?")
    public void publish() throws IOException, ParseException {
        simService.writeCsv();
    }

}