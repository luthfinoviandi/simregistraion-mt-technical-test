package com.luthfi.constant;

public enum SIMTypeConst {
    PREPAID("PREPAID"), POSTPAID("POSTPAID");

    private String value;

    private SIMTypeConst(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}