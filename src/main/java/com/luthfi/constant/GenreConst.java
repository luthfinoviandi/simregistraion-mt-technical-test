package com.luthfi.constant;

public enum GenreConst {
    FEMALE("F"), MALE("M");

    private String value;

    private GenreConst(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}