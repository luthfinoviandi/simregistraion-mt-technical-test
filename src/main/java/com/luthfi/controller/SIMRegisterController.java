package com.luthfi.controller;

import com.luthfi.service.SIMService;
import com.luthfi.util.CsvUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/register-sim")
public class SIMRegisterController {
    private static final Logger logger = LoggerFactory.getLogger(CsvUtil.class);

    @Autowired
    SIMService simService;

    @PostMapping(value = "/register")
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> register(@RequestParam("file")MultipartFile file) throws ParseException, IOException {
        Map<String, Object> data = new HashMap<String, Object>();

        if(file.getOriginalFilename().isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("No selected file to upload!");
        }

        if(!CsvUtil.isCSVFile(file)){
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("This is not CSV file!");
        }

        try {
            // generate txt file
            simService.register(file.getInputStream());
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("error");
        }


        return ResponseEntity.status(HttpStatus.OK).body("success");
    }
}
